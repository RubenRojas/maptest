#!/bin/bash
BASEDIR=$(dirname $0)
cd ${BASEDIR}

git config credential.helper store
git status
git add .


echo "Mensaje del Commit: "
read msg

git commit -m "Auto Commit MacOs [$(date +"%x %r")] : $msg"
git push origin master

exit 0