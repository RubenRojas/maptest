package cl.ruben.maptest.interfaz;

import java.util.List;

import cl.ruben.maptest.model.PoiModel;

public interface ViewMapa {
    //Metodos que tiene la vista y que estan disponibles para el presenter
    void displayPoiList(List<PoiModel> list);
}
