package cl.ruben.maptest.interfaz;

import cl.ruben.maptest.model.PoiModel;

public interface ViewDetallePoi {
    void showData(PoiModel poiModel);
}
