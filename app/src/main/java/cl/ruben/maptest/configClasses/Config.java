package cl.ruben.maptest.configClasses;

import com.google.android.gms.maps.model.LatLng;

public class Config {
    public static double initLat = -33.438023;
    public static double initLon = -70.650480;
    public static LatLng santiago = new LatLng(Config.initLat, Config.initLon);
    public static String TAG = "DEVELOP";
    public static int MinPoi = 10;
}
