package cl.ruben.maptest.configClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class  AdminSQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "MapTest";
    private static final int DATABASE_VERSION = 2;
    public static String TAG = "DEVELOP_SQLITE";
    private AdminSQLiteHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static AdminSQLiteHelper getInstance(Context ctx) {
        return new AdminSQLiteHelper(ctx.getApplicationContext());
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "On Create");
        DELETE_TABLES(db);

        String query="create table poi " +
                "(id int primary key, " +
                "nombre text, "+
                "img text, "+
                "resenia text, "+
                "descripcion text, "+
                "lat real, "+
                "lon real "+
                ")";

        db.execSQL(query);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        DELETE_TABLES(db);
        //tabla de usuario
        String query="create table poi " +
                "(id int primary key, " +
                "nombre text, "+
                "img text, "+
                "resenia text, "+
                "descripcion text, "+
                "lat real, "+
                "lon real "+
                ")";


        db.execSQL(query);
    }
    public static void DELETE_TABLES(SQLiteDatabase db){
        String[] tablas = {
                "poi",
        };

        for (int i = 0; i < tablas.length; i++) {
            String query = "drop table if exists ";
            query += tablas[i]+"";
            db.execSQL(query);
        }
    }

}

