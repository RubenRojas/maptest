package cl.ruben.maptest.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cl.ruben.maptest.configClasses.AdminSQLiteHelper;
import cl.ruben.maptest.configClasses.Config;

public class PoiModel {
    int id;
    String nombre;
    String img;
    String resenia;
    double lat;
    double lon;
    String descripcion;


    static String TAG = Config.TAG;

    public PoiModel() {
    }

    public PoiModel(int id, String nombre, String img, String resenia, double lat, double lon, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.img = img;
        this.resenia = resenia;
        this.lat = lat;
        this.lon = lon;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getResenia() {
        return resenia;
    }

    public void setResenia(String resenia) {
        this.resenia = resenia;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "PoiModel{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", img='" + img + '\'' +
               // ", resenia='" + resenia + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
               // ", descripcion='" + descripcion + '\'' +
                '}';
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    public PoiModel fromJson(JSONObject json){

        PoiModel poi = new PoiModel();
        try {
            poi.id = json.getInt("id");
            poi.nombre = json.getString("nombre");
            poi.img = json.getString("img");
            poi.resenia = json.getString("resenia");
            poi.lat = json.getDouble("lat");
            poi.lon = json.getDouble("lon");
            poi.descripcion = json.getString("descripcion");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return poi;
    }





    /*******************************************/
    //      Database Functions
    /*******************************************/


    public ContentValues toCV(){
        ContentValues values = new ContentValues();
        values.put("id", this.getId());
        values.put("nombre", this.getNombre());
        values.put("img", this.getImg());
        values.put("resenia", this.getResenia());
        values.put("lat", this.getLat());
        values.put("lon", this.getLon());
        values.put("descripcion", this.getDescripcion());
        return values;
    }

    public PoiModel fromCursor(Cursor cursor){
        PoiModel tmp = new PoiModel();

        tmp.setId(cursor.getInt(cursor.getColumnIndexOrThrow("id")));
        tmp.setLat(cursor.getDouble(cursor.getColumnIndexOrThrow("lat")));
        tmp.setLon(cursor.getDouble(cursor.getColumnIndexOrThrow("lon")));

        tmp.setImg(cursor.getString(cursor.getColumnIndexOrThrow("img")));
        tmp.setNombre(cursor.getString(cursor.getColumnIndexOrThrow("nombre")));
        tmp.setResenia(cursor.getString(cursor.getColumnIndexOrThrow("resenia")));
        tmp.setDescripcion(cursor.getString(cursor.getColumnIndexOrThrow("descripcion")));


        return tmp;
    }

    public void insertPois(JSONArray PoiArray, Context ctx){
        for(int i=0; i< PoiArray.length(); i++){
            try {
                PoiModel tmp = fromJson(PoiArray.getJSONObject(i));
                if(tmp.insert(ctx)){
                    Log.d(TAG, "insertPois: SUCCESS, ID: "+tmp.id);
                }
                else{
                    Log.d(TAG, "insertPois: FAIL, id: "+tmp.id);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getPoiCount(Context ctx){
        int ret = 0;
        AdminSQLiteHelper admin = AdminSQLiteHelper.getInstance(ctx);
        SQLiteDatabase bd = admin.getWritableDatabase();

        String query = "select count(*) as cant from poi";
        Cursor cursor = bd.rawQuery(query, null);
        if(cursor.moveToFirst()){
            ret = cursor.getInt(0);
        }
        cursor.close();
        bd.close();
        return ret;
    }

    public boolean insert(Context ctx) {
        int poiTotales = getPoiCount(ctx);

        if(poiTotales <=10){
            if(this.getPoiByName(this.nombre, ctx) == null){
                AdminSQLiteHelper admin = AdminSQLiteHelper.getInstance(ctx);
                SQLiteDatabase bd = admin.getWritableDatabase();
                ContentValues values = toCV();
                Log.d(TAG, "insert: VALUES: "+values.toString());

                boolean createSuccessful = bd.insert("poi", null, values) > 0;
                bd.close();
                return createSuccessful;

            }
            else{
                return false;
            }
        }
        else{
            return false;
        }




    }

    public List<PoiModel> getList(Context ctx){
        Log.d(TAG, "getList: Llamada en el MODELO");

        AdminSQLiteHelper admin = AdminSQLiteHelper.getInstance(ctx);
        SQLiteDatabase bd = admin.getWritableDatabase();

        List<PoiModel> retData = new ArrayList<>();
        String query = "SELECT * from poi";
        Cursor cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {
            retData.add(fromCursor(cursor));

        }
        cursor.close();
        bd.close();
        return retData;
    }

    public PoiModel getPoiByName(String nombre, Context ctx){

        String query = "select * from poi where nombre='"+nombre+"'";
        Log.d(TAG, "getPoiByName: "+query);
        AdminSQLiteHelper admin = AdminSQLiteHelper.getInstance(ctx);
        SQLiteDatabase bd = admin.getWritableDatabase();


        Cursor fila = bd.rawQuery(query, null);
        PoiModel tmp = new PoiModel();
        if(fila.moveToFirst()){
            return fromCursor(fila);
        }
        else{
            return null;
        }
    }

}
