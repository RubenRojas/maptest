package cl.ruben.maptest.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PoiData {
    public JSONArray Array;

    public PoiData() {
        //Creo JSON con POI para agregar a la base de datos
        JSONObject poi_1 = new JSONObject();
        try {
            poi_1.put("id", 1);
            poi_1.put("descripcion", "La plaza de Armas de Santiago se ubica en la comuna de Santiago y es el núcleo del centro histórico de la capital de Chile.");
            poi_1.put("nombre", "Plaza de Armas de Santiago");
            poi_1.put("img", "https://upload.wikimedia.org/wikipedia/commons/3/3e/Plaza_de_Armas.JPG");
            poi_1.put("resenia", "La capital chilena se fundó el 12 de febrero de 1541 por el conquistador Pedro de Valdivia. Su trazado en forma de damero, semejante a un tablero de ajedrez, fue diseñado por el alarife Pedro de Gamboa.8\u200B De esta forma se planificó la construcción de una plaza central en torno a la cual se erigieran los principales edificios administrativos.\n" +
                    "\n" +
                    "En torno a la plaza nacieron las recovas o mercados, pues las carretas con mercancías llegaban a esta zona durante la época de la Colonia. Al medio, se ubicaba la horca para ejecutar a los sentenciados y demostrar el poder de la Justicia Real.9\u200B\n" +
                    "\n" +
                    "En los años siguientes, la idea de una cuadra totalmente llana cambió y en 1859, siguiendo conceptos arquitectónicos europeos, la plaza fue forestada al instalar árboles y bellos jardines. Posteriormente, en 1896 el paisajista francés avecindado en Chile Guillermo Renner diseñó un jardín pintoresco, con lagunas de agua y paseos sinuosos. Entre 1998 y 2000, una renovación de la zona debido a las obras de construcción de la estación de Metro Plaza de Armas dio origen a la actual plaza, que mezcla sectores de explanada para actividades culturales, especialmente la de los clásicos pintores y humoristas, jardines y una pérgola central para la ejecución musical de la banda municipal.");
            poi_1.put("lat", -33.438023);
            poi_1.put("lon", -70.650480);


        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_2 = new JSONObject();
        try {
            poi_2.put("id", 2);
            poi_2.put("descripcion", "El cerro Santa Lucía es un parque urbano ubicado en el corazón de la ciudad de Santiago de Chile. Tiene una altitud de 629 msnm.");
            poi_2.put("nombre", "Cerro Santa Lucía");
            poi_2.put("img", "https://upload.wikimedia.org/wikipedia/commons/f/fc/Cerrosantalucia.jpg");
            poi_2.put("resenia", "Se cree que los aborígenes lo llamaban Huelén, que significaría \"dolor, desdicha\",1\u200B aunque su verdadera etimología aún no ha sido dilucidada y es controversial hasta el día de hoy.2\u200B Otras posibilidades son que provenga de la palabra welen, a su vez derivada de la voz wele, pero con el significado de \"desgracia, mal presagio\".3\u200B Es posible también que no sea una palabra de origen mapuche, sino que se trate de otra lengua local.4\u200B Pedro de Valdivia lo bautizó como Santa Lucía por haber llegado él al punto del valle en que fundaría la ciudad el 13 de febrero de 1541, " +
                    "día que recuerda a Santa Lucía de Siracusa. Según algunos historiadores, justo después de llegar, comienza el proceso de expropiación al cacique Huelén Huala, a quien manda a Apoquindo.");
            poi_2.put("lat", -33.440278);
            poi_2.put("lon", -70.644167);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject poi_3 = new JSONObject();
        try {
            poi_3.put("id", 3);
            poi_3.put("descripcion", "El cerro San Cristóbal —antiguamente Tupahue- está ubicado en Santiago, la capital de Chile. Con una altitud de 880  y una prominencia de 280 m, es el segundo punto más alto de la ciudad, superado por el cerro Renca. El cerro se encuentra entre las comunas de Providencia y Recoleta, y tiene a sus pies al Barrio Bellavista.");
            poi_3.put("nombre", "Cerro San Cristóbal");
            poi_3.put("img", "https://upload.wikimedia.org/wikipedia/commons/2/20/San_Crist%C3%B3bal_altura.jpg");
            poi_3.put("resenia", "El nombre original del cerro era Tupahue (en quechua: Centinela)​ —actualmente, se conoce como Tupahue o Chacarillas al cerro ubicado en el sector norte del Parque Metropolitano de Santiago, entre el Jardín Botánico y la Avenida Américo Vespucio—." +
                    " Fue rebautizado en honor a san Cristóbal de Licia, patrono de los viajeros, por el conquistador Pedro de Valdivia. La primera referencia escrita de su nombre actual data del siglo XVIII, " +
                    "cuando el gobernador de Chile Manuel de Amat y Junyent hizo una descripción geográfica del territorio al rey Carlos III.1​ Durante la época colonial, diversas canteras operaron en la ladera sur, " +
                    "de las que se sacaron piedras para el Puente de Cal y Canto, el Palacio de La Moneda y para adoquinar las calles de Santiago. En 1903, con fondos donados por el filántropo estadounidense D. O. Mills, " +
                    "se instaló en una de sus cumbres el Observatorio Astronómico Mills,3​ actualmente denominado «Manuel Foster»,4​ gemelo del Observatorio Astronómico Lick de la Universidad de California. " +
                    "En su época —con 37 pulgadas de diámetro, 6 pulgadas de espesor y 500 libras de peso—,3​ su telescopio reflector fue el más grande del hemisferio Sur y el décimo en tamaño del mundo. " +
                    "Esta instalación pertenece en el presente a la Pontificia Universidad Católica de Chile, que lo mantiene pese a su inviabilidad científica producto de la contaminación lumínica emanada por la ciudad. " +
                    "El santuario de la Inmaculada Concepción, con su gran estatua en la cumbre del cerro, fue inaugurado el 26 de abril de 1908,4​ por iniciativa del arzobispo de Santiago Mariano Casanova, gracias a donaciones particulares. Esta estatua, visible desde gran parte de la ciudad, es considerada como un símbolo de Santiago. En agosto de 1916, Alberto Mackenna Subercaseaux, intendente de Santiago y presidente de los boy scouts, junto con el senador Pedro Bannen iniciaron una campaña de expropiación del Cerro a privados con la idea de convertirlo en un gran parque público.4​ Dentro de las primeras obras para ese fin se encontraba un programa de forestación iniciado en 1921, ya que el cerro estaba naturalmente desprovisto de vegetación. En los años siguientes, se construyen canales de regadíos, caminos, el funicular (1925), el Zoológico Nacional, el Torreón Victoria y la Capilla del Santuario de la Cumbre (1931). Durante los años 1960, se continuó el desarrollo del Cerro con la plaza México, la piscina Tupahue, la Casa de la Cultura Anahuac y los juegos infantiles Gabriela Mistral. En 1966 una ley consolidó los Servicios Cerro San Cristóbal y Jardín Zoológico Nacional en una sola entidad que se conoce como Parque Metropolitano de Santiago. En su cima, el anfiteatro y su santuario acogieron al papa Juan Pablo II en su visita de abril de 1987. También en este lugar se encuentran las antenas difusoras de televisión. A lo anterior, desde diciembre de 2006, se encuentra un Memorial (Columbario), que pertenece a la funeraria «Acoger», donde se encuentran las cenizas de mucha gente fallecida. ");
            poi_3.put("lat", -33.425278);
            poi_3.put("lon", -70.633333);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_4 = new JSONObject();
        try {
            poi_4.put("id", 4);
            poi_4.put("descripcion", "El Palacio de La Moneda, comúnmente conocido como La Moneda, es la sede del presidente de la República de Chile. También alberga la Secretaría General de la Presidencia, la Secretaría General de Gobierno y el Ministerio de Ciencia. Se ubica en la comuna de Santiago, entre las calles Moneda (norte), Morandé (este), la Alameda del Libertador Bernardo O'Higgins (sur) y Teatinos (oeste). Al norte se ubica la plaza de la Constitución y al sur, la de la Ciudadanía.");
            poi_4.put("nombre", "Palacio de La Moneda");
            poi_4.put("img", "https://upload.wikimedia.org/wikipedia/commons/2/21/Palacio_de_La_Moneda_-_miguelreflex.jpg");
            poi_4.put("resenia", "El palacio fue originalmente proyectado para albergar la Casa de Moneda en la época colonial chilena. En vista que las autoridades locales de entonces, no contaban con los recursos para establecer una casa de acuñación, así como tampoco la Corona Española, el rey Felipe V, por Real Cédula de 1° de octubre de 1743, decidió conceder a un particular la realización del proyecto, quien asumiría las eventuales pérdidas y ganancias. Fue así como la Real Casa de Moneda se estableció en un solar distinto del actual, y acuñó la primera moneda el 10 de septiembre de 1749, gracias al financiamiento de Francisco García de Huidobro, I marqués de Casa Real.4​ Posteriormente, el rey Carlos III decidió que la Real Casa de Moneda de Santiago fuera administrada por funcionarios de la Corona. El Gobernador del Reino, en nombre del monarca, tomó posesión del establecimiento en 1772. En compensación, García de Huidobro fue nombrado tesorero perpetuo de la Casa de Moneda. El establecimiento se trasladó al antiguo colegio de los jesuitas, que estaba desocupado luego de su expulsión; pero pronto fue claro que se debía construir un local apropiado. Se hicieron planos para ello, que se enviaron para su aprobación al virrey del Perú, quien los rechazó y ordenó hacer otros nuevos. Por esa época, el obispo de Santiago, Manuel de Alday, estaba trabajando en las obras de la nueva catedral y solicitaba a España colaboración para concluir la obra. Uno de los destinatarios de sus ruegos, fue el abate Pedro Toesca, ecónomo del Colegio Cardenalicio en Roma, quien hizo llegar la solicitud a su hermano Joaquín. Era este uno de los arquitectos que desde Italia pasó a España, cuando Carlos III accedió al trono, como ayudante de Francisco Sabatini, quien asumió numerosos proyectos, entre ellos la continuación del Palacio Real de Madrid. Ayudante del gran taller madrileño de Sabatini, Toesca habría realizado allí su vocación de arquitecto, de no haberse empeñado el Obispo de Santiago de Chile en concluir su Catedral.​ Joaquín Toesca y Ricci, quien sería el autor del palacio de La Moneda de Santiago, nació en Roma en 1752.5​ Se educó en Milán, Barcelona y en su ciudad natal, iniciándose en el estudio de la arquitectura con Sabatini, quien llegó a ser su maestro y amigo, como ya se ha señalado. Toesca arribó a Santiago a comienzos de 1780 y de inmediato se dedicó a las obras de la Catedral. Su excelente trabajo llevó al gobernador Agustín de Jáuregui, en junio de ese año, a pedirle que realizara un proyecto para la Real Casa de Moneda, que se ubicaría en un solar próximo al río Mapocho, y al puente de Cal y Canto. Toesca elaboró los planos y los presentó al gobernador en 1782. Un juego de 13 planos fue enviado al virrey de Lima para su aprobación, como era obligatorio en aquel tiempo. Mientras tanto, se iniciaron las obras de cimentación del edificio, pero a un metro de profundidad aguas subterráneas anegaron la obra, que fue paralizada, en enero de 1784 por el nuevo gobernador Ambrosio Benavides. Toesca debió buscar un nuevo emplazamiento para la obra. De entre los varios propuestos, el preferido por el arquitecto fue el sitio del Colegio Carolino, que había pertenecido de los jesuitas, conocido como solar de los teatinos por la comunidad religiosa que allí residió. El rector del Colegio firmó a mediados de 1784 un acuerdo para el traspaso del sitio.6​ En enero de 1786 comenzaron las obras y un año después llegaron los materiales: cal de la hacienda Polpaico; arenas del río Maipo; piedras de la cantera colorada del cerro San Cristóbal; madera de roble y ciprés de los bosques valdivianos; cerrajería y forja española de Vizcaya; y 20 variedades de ladrillos horneados en Santiago para la construcción de dinteles, esquinas, pisos, molduras y los sólidos muros de más de un metro de espesor. La rejería, sólida como para un castillo medieval y fundamental para proteger los caudales del Reino, fue minuciosamente dibujada por Toesca.4​ Durante el gobierno de Ambrosio O'Higgins —quien consideraba que el edificio superaba en mucho la obra para la cual estaba destinada, pero que no por ello dejó de apoyar al arquitecto— Toesca encargó a España el resto de los materiales, que fueron traídos en la fragata El África y llegaron al puerto de Valparaíso en marzo de 1792. El listado comprendió: 104 rejas para ventanas, 42 balcones, chapas y pestillos, 620 clavos de media vara, 5500 clavos de tercio, 18 000 de cuarto y 28 quintales de clavos medio tillado. Sus paredes se construyeron de ladrillos gruesos unidos con mortero de cal y arena del río, dándoles más de 1 metro de espesor, para que el edificio soportara los terremotos de Santiago, ciudad altamente sísmica. Todas las obras que proyectó y ejecutó reunían, a la belleza y solidez consultadas en los planos, una esmerada ejecución, que les ha permitido desafiar el tiempo y las conmociones de la tierra, señalaba sobre Toesca el historiador Francisco Antonio Encina.8​ Joaquín Toesca murió en el 11 de junio de 1799, a los 47 años de edad, sin ver terminada la Real Casa de Moneda; en su reemplazo fue nombrado el ingeniero militar Agustín Cavallero, quien siguió las directrices de Toesca, y confeccionó los planos más antiguos que se conservan del edificio (de 1800), dado que los del propio Toesca desaparecieron. Luego que el Rey trasladara a Cavallero a Panamá, en 1802, a cargo del inconcluso palacio quedaron Miguel María Atero e lgnacio de Andía Varela, chileno este último, discípulos ambos de Toesca y Cavallero. A partir de 1805 realizó obras en las terminaciones del edificio otro seguidor del italiano, el criollo Juan José de Goycoolea, quien concluyó, entre otras cosas, la capilla y los pilones de piedra y cobre que se colocaron frente a la fachada principal, en la plazuela formada a instancias del Conde de la Quinta Alegre para dar perspectiva al palacio, para lo que se adquirieron y demolieron las casas de la acera norte de la calle llamada desde entonces, de la Moneda Nueva.​ Esta plazuela, que ocupaba aproximadamente un cuarto de su manzana, existió de 1805 a 1935, cuando sus construcciones fueron demolidas para dar paso a la actual plaza de la Constitución. Después de 25 años de obras, La Moneda fue oficialmente inaugurada en 1805 por el gobernador de la época, Luis Muñoz de Guzmán, a pesar de que parte del edificio quedó inconclusa por largo tiempo. Desde 1798 dirigía el establecimiento, como superintendente de la Casa de Moneda, José Santiago Portales, quien usaba como habitación para sí y su familia de 22 hijos, el amplio departamento que le correspondía en el segundo piso del palacio. Durante la Reconquista, el superintendente, que había sido partidario de la Independencia, fue destituido y desterrado al archipiélago de Juan Fernández, y luego a Melipilla. Tras la victoria de Chacabuco, el director supremo Bernardo O'Higgins lo repuso en su cargo. Así, al superintendente Portales le correspondió dirigir la acuñación de las primeras monedas del Chile independiente.");
            poi_4.put("lat", -33.443018);
            poi_4.put("lon", -70.65387);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_5 = new JSONObject();
        try {
            poi_5.put("id", 5);
            poi_5.put("descripcion", "La Casa Colorada es uno de los principales ejemplos que se conservan de la casa colonial chilena. Es una casa colonial construida con muros de piedra sillar, techumbre de madera en roble, y canelo en los entrepisos con armazones de coligüe.");
            poi_5.put("nombre", "Casa Colorada");
            poi_5.put("img", "https://upload.wikimedia.org/wikipedia/commons/c/c4/Casa_Colorada_02.JPG");
            poi_5.put("resenia", "Esta vivienda fue construida para el presidente de la primera junta de gobierno, don Mateo de Toro y Zambrano, que vivió ahí hasta su muerte. Este compró el solar en el año 1769, encomendándole al portugués Joseph de la Vega su construcción, la que comenzó el mismo año. La construcción tardó 10 años, pero a pesar de la tardanza, el resultado fue muy elogiado. En su época fue el único edificio particular con fachada de piedras y de dos pisos. Aunque a través de diversos medios se sigue difundiendo, es un error afirmar que en la Casa Colorada se llevó a cabo la Primera Junta de Gobierno de 1810. Este magno evento, escogido para celebrar las Fiestas Patrias, se realizó en realidad en el Tribunal del Consulado, que estuvo ubicado donde ahora se ubica el Palacio de los Tribunales de Justicia (entre las calles Bandera y Morande). La Casa Colorada fue la residencia de los Condes de la Conquista hasta el fallecimiento de la IV Condesa, Doña Nicolasa de Toro-Zambrano y Dumont de Holdre de Correa de Saa. A finales del siglo XIX, pasó a sus hijos, los Correa y Toro, manteniéndose en manos de la familia hasta mediados del siglo XX. Actualmente la Casa Colorada alberga el Museo de Santiago, encontrándose en muy buen estado. Allí se exhiben muestras relacionadas con la fundación de Santiago y su desarrollo a lo largo del tiempo. El Museo de Santiago-Casa Colorada se encuentra cerrada en un proceso de remodelación y restauración que todavía no empieza después del terremoto del 14 de febrero de 2010.");
            poi_5.put("lat", -33.438611);
            poi_5.put("lon", -70.649167);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_6 = new JSONObject();
        try {
            poi_6.put("id", 6);
            poi_6.put("descripcion", "El palacio del ex Congreso Nacional de Chile fue la sede que albergó las dos cámaras del Congreso Nacional desde 1876 hasta 1973. " +
                    "Ubicado en Santiago de Chile, fue declarado Monumento Histórico en 1976");
            poi_6.put("nombre", "Palacio del ex Congreso Nacional de Chile");
            poi_6.put("img", "https://upload.wikimedia.org/wikipedia/commons/5/5a/Excongreso.jpg");
            poi_6.put("resenia", "La construcción del congreso nacional se proyectó durante el gobierno de Manuel Montt.2​ Los terrenos donde está construido pertenecieron a los Jesuitas. Los planos del nuevo edificio fueron realizados por el arquitecto francés Claude-François Brunet de Baines,3​ quien fue contratado por el gobierno en 1848 y fallecido en 1855. En 1857 se iniciaron las obras a cargo de Lucien Hénault, pero en 1860, fueron interrumpidas por falta de recursos. En 1870, el ingeniero chileno Manuel Aldunate prosigue con las obras. Terminada finalmente por el arquitecto italiano Eusebio Chelli. El edificio del Congreso Nacional fue inaugurado, aún inconcluso,1​ el 1 de junio de 1876, durante el gobierno de Federico Errázuriz Zañartu.4​ La construcción está ubicada entre la manzana comprendida entre las calles Bandera, Compañía, Catedral y Morandé. El ala poniente fue ocupada por el Senado y el ala oriente por la Cámara de Diputados. Entre ambas se encuentra el Salón de Honor, lugar de reunión del Congreso Pleno. En 18955​ un incendio destruye gran parte del edificio, siendo restaurado al año siguiente por Carlos Bunot con la colaboración de los arquitectos Doyère, Joannon y von Moltke. La reinauguración fue en 1901, bajo el gobierno de Federico Errázuriz Echaurren. El terremoto producido en 1906 le causó severos daños al edificio, por lo que debió ser reacondicionado una vez más, en esta ocasión por el arquitecto chileno Alberto Cruz Montt.2​ Desde esa fecha el Senado y la Cámara de Diputados funcionaron hasta el golpe de estado de 1973.4​ Desde esa fecha se utilizó para fines del Ministerio de Justicia. Con el retorno a la democracia en 1990, y por motivos de descentralización la sede del Congreso se trasladó a la ciudad de Valparaíso. En las antiguas dependencias se instaló hasta el 2006 la Cancillería. El Ministerio de Relaciones Exteriores abandonó las dependencias quedando en manos del Ministerio de Bienes Nacionales. El 19 de mayo de 2006, y a través de una resolución exenta de este ministerio, se selló la restitución definitiva del edificio a las autoridades del Congreso.6​ En la actualidad, posee en su biblioteca algunos libros consultados por los Senadores y Diputados de la Nación. Además, los parlamentarios suelen reunirse allí y en su Salón de Honor se realizan importantes nombramientos.");
            poi_6.put("lat", -33.43832);
            poi_6.put("lon", -70.653429);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject poi_7 = new JSONObject();
        try {
            poi_7.put("id", 7);
            poi_7.put("descripcion", "La Basílica del Salvador es un templo católico chileno de arquitectura neogótica, ubicado en la esquina de las calles Huérfanos y Almirante Barroso en Santiago de Chile. Debido a los terremotos que afectaron al centro de Chile en 1985 y en 2010, se encuentra en un estado deplorable.");
            poi_7.put("nombre", "Basílica del Salvador");
            poi_7.put("img", "https://upload.wikimedia.org/wikipedia/commons/f/fa/Bas%C3%ADlica_del_Salvador_02.JPG");
            poi_7.put("resenia", "De estilo neogótico con semblanzas románicas y germánicas, la Basílica del Salvador tiene tres naves paralelas de la misma altura, rasgo arquitectónico único en Chile. Las naves laterales rematan en sendas pequeñas capillas a los costados del altar principal. En el transepto se ubica el coro a la altura del triforio. El interior está iluminado a través de vidrieras de gran calidad, procedentes de Bélgica. Tanto las columnas lobuladas como los muros y bóvedas están profusamente decorados, con predominio de los dorados. La fachada principal se caracteriza por su albañilería de ladrillo a la vista.1​ De tamaño monumental, la Basílica tiene unas dimensiones de 98 m de largo por 37 de ancho y tiene una altura interior de 30 m, contando con una capacidad de cinco mil personas, sólo comparable a la Catedral de Santiago de Chile.La primera piedra de la Basílica se colocó tras la destrucción de la Iglesia de la Compañía, estando el proyecto original encargado al ingeniero alemán Teodoro Burchard en 1871. Luego pasó a manos del arquitecto chileno Josué Smith Solar, quedando la obra terminada en 1932, después de sesenta años de trabajos. El papa Pío XI elevó el templo al rango de Basílica en 1937.2​ El templo tuvo su apogeo a la par del barrio Brasil (entre 1925 y 1940), cuando la elite del sector acudía ahí. Hasta 1984, la procesión de la Virgen del Carmen salía desde su interior. Fue nombrada Monumento Nacional en 1977. Se encuentra en un pésimo estado de conservación debido a los graves daños producidos por el terremoto del 3 de marzo de 1985. Los resultados del sismo fueron el agrietamiento de una pared oeste, la caída de parte de las bóvedas sobre las butacas y la pérdida del estuco en su fachada. Variados intentos de restauración no han dado frutos. El terremoto del 27 de febrero de 2010 agravó aún más la situación del edificio, pues destruyó parte del techo, un muro lateral y varios de los vitrales de cuatro metros que fueron traídos desde Múnich en el siglo XIX.3​ En 2011 se anunció que el Ministerio de Obras Públicas licitará un proyecto para diseñar una estructura anexa que soporte estructuralmente la iglesia, para de esta forma evaluar un proyecto de remodelación a futuro. Éste incluiría la confección de un registro fotográfico de las ornamentaciones que aún se mantienen al interior de la iglesia y un catastro de los vitrales aún están en buen estado.                     ");
                    poi_7.put("lat", -33.4413);
            poi_7.put("lon", -70.6618);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_8 = new JSONObject();
        try {
            poi_8.put("id", 8);
            poi_8.put("descripcion", "El Cementerio General de Santiago de Chile, se ubica en la comuna de Recoleta. Cuenta con 86 hectáreas, donde se encuentran cerca de dos millones de personas sepultadas.");
            poi_8.put("nombre", "Cementerio General de Santiago");
            poi_8.put("img", "https://upload.wikimedia.org/wikipedia/commons/8/86/Cementerio_General._Acceso_por_Avenida_La_paz..JPG");
            poi_8.put("resenia", "El cementerio fue nacido el 9 de diciembre de 1821 por el Director Supremo Bernardo O'Higgins Riquelme. La construcción del cementerio fue posible gracias a la cesión de los derechos de extracción de hielo para las heladerías. Originalmente no se podían enterrar a los protestantes, llamados entonces «disidentes», y recién en 1854 se creó el Patio de los Disidentes Nº 1. El decreto de cementerios de 1871 estableció la sepultura sin distinción de credo en un espacio debidamente separado para los disidentes y permitió la creación de cementerios laicos con fondos fiscales o municipales que debían ser administrados por el Estado o el municipio. El 2 de agosto de 1883, se promulgó la ley de cementerios civiles (como parte de las leyes laicas) bajo la presidencia de Domingo Santa María González. Estableció la administración de los cementerios públicos por el Estado o municipio y fue retirada cualquier administración eclesiástica, la no discriminación en la sepultura de los difuntos y prohibió el entierro en los terrenos de las iglesias. La autoridad eclesiástica creó el Cementerio Católico de Santiago en 1883 y lo clausuró ese mismo año. Fue reabierto en 1890. Se destaca de él su riqueza en arquitectura con mausoleos de estilo egipcio, griego, mesoamericano, gótico, morisco, etc. además de esculturas, vitrales y jardines que hacen del Cementerio un patrimonio cultural de gran valor. Estructuras de importancia son el Mausoleo Histórico de Chile, el Memorial del Detenido Desaparecido y del Ejecutado Político y el Memorial por la diversidad de Chile. Al interior del Cementerio General se encuentran ubicados el Cementerio Parque Las Encinas, y en los alrededores, el Cementerio Católico y el Cementerio Israelita de Santiago. La estación de Metro Cementerios se encuentra ubicada frente a la entrada de Avenida Recoleta. Cuenta con un mausoleo circense, único en el mundo, donde descansan los restos de artistas circenses chilenos desde 1942. Actualmente está diseñado bajo una gran carpa de circo luego de las remodelaciones del 2012. Además cuenta con un mausoleo trans, único en Latinoamérica, iniciativa que surge en 2013 y se concreta en 2018, destinada a personas transgéneros.1​");
            poi_8.put("lat", -33.413786);
            poi_8.put("lon", -70.648753);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_9 = new JSONObject();
        try {
            poi_9.put("id", 9);
            poi_9.put("descripcion", "El palacio Cousiño es un palacio residencial ubicado en la ciudad de Santiago (Chile). El palacio cuenta con 3.500 metros cuadrados construidos, dos pisos y 12 salones. Fue la primera propiedad en Sudamérica en contar con generador eléctrico (comprado por la Familia a Thomas Edison, amigo de la familia)");
            poi_9.put("nombre", "Palacio Cousiño");
            poi_9.put("img", "https://upload.wikimedia.org/wikipedia/commons/4/48/Palacio_Cousi%C3%B1o.JPG");
            poi_9.put("resenia", "El palacio Cousiño fue el hogar de la familia Cousiño-Goyenechea, una de las más acaudaladas familias de Santiago, quienes eran dueños de la mina de carbón de Lota, la mina de plata de Chañarcillo y la viña Cousiño-Macul, entre otras. El palacio se comenzó a construir en un terreno de 11 000 m² en 18701​ para el matrimonio compuesto por Luis Cousiño, fallecido en Perú en 1873 aquejado de tuberculosis, e Isidora Goyenechea. Tuvieron seis hijos: (originalmente eran ocho, Isidora que falleció al nacer y Alfredo fallecido a la edad de un año) Luis Alberto, Carlos Roberto, Luis Arturo, Adriana, Loreto y María Luz. Las obras terminaron en 1878 pero fue inaugurado en 1882. La construcción fue encargada al arquitecto francés Paul Lathoud, responsable también de la construcción del edificio del Museo Nacional de Historia Natural. Para la decoración y construcción del palacio se trajeron de Europa en los barcos de la familia terciopelos, brocados, porcelanas de Sèvres, Limoges y Meissen, parqué tallado a mano de nogal, caoba, roble americano, ébano, haya alemana y otros. Cortinajes bordados a mano en Francia y pisos de mayólica italiana. Fue la primera propiedad en Sudamérica en poseer un generador eléctrico, comprado a Thomas Edison, amigo de la familia —Isidora Goyenechea cedió la energía eléctrica al vecindario del palacio— y la primera también en tener, gracias a su sistema de calefacción, agua caliente y agua fría simultáneamente. Tres generaciones de la familia Cousiño habitaron el palacio por seis décadas, hasta 1928.1​ En 1940 fue sacado a remate con parte de su amueblado y pertenencias. En ese entonces el alcalde de Santiago, el señor Pacheco Sty, llegó a un acuerdo con la familia, prácticamente donó la propiedad a la municipalidad de Santiago por la suma de tres millones de monos en bonos, con la condición de que el palacio se conservara como tal. El 12 de octubre de 1968, un incendio destruyó completamente el segundo piso.1​ En 1977 el alcalde Patricio Mekis abrió el palacio como museo y en 1981 fue declarado Monumento Nacional.1​ El Palacio fue usado para alojar a grandes personajes que visitaban Santiago. El gobierno chileno recibió en este palacio a la canciller de Israel Golda Meir, a los presidentes Adolfo López Mateos de México, Heinrich Lübke de Alemania, Giuseppe Saragat de Italia, Charles De Gaulle de Francia, el rey Balduino de Bélgica, entre otros. También pretendía recibir a la reina Isabel II de Inglaterra, quien visitaría Chile en noviembre de 1968, un mes después del incendio, pero la soberana debió ser alojada en el ex-Hotel Carrera. Se reconstruyó entre 1970 y 1980 alojándose a los presidentes João Baptista Figueiredo de Brasil en 1980 y Gregorio Álvarez de Uruguay en 1982. ");
            poi_9.put("lat", -33.452192);
            poi_9.put("lon", -70.656611);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_10 = new JSONObject();
        try {
            poi_10.put("id", 10);
            poi_10.put("descripcion", "El Museo de la Educación Gabriela Mistral (MEGM) fue fundado el 13 de septiembre de 1941 y funciona en el ala oeste de la antigua Escuela Normal N° 1 de Santiago, ubicada en la intersección de las calles Chacabuco y Compañía de Jesús, en el Barrio Yungay de la comuna de Santiago, Chile.");
            poi_10.put("nombre", "Museo de la Educación Gabriela Mistral");
            poi_10.put("img", "https://upload.wikimedia.org/wikipedia/commons/f/f9/Museo_de_la_Educaci%C3%B3n_Gabriela_Mistral.jpg");
            poi_10.put("resenia", "El antecedente más directo del actual Museo de la Educación Gabriela Mistral es la Exposición Retrospectiva de la Enseñanza que se organizó en el Museo de Bellas Artes el año 1941, como parte de las celebraciones en conmemoración del cuarto centenario de la fundación de la ciudad de Santiago. Esta exposición revisaba la historia de la educación primaria, secundaria y universitaria en Chile desde el periodo colonial hasta 1941 desde diversas temáticas y se compuso principalmente de afiches, material didáctico, cuadros artísticos y datos estadísticos, entre otros. Su organizador fue Carlos Stuardo Ortiz. Sin embargo, las intenciones de establecer un museo de estas características se había hecho notar desde fines del siglo XIX, cuando José Abelardo Núñez organizó la Exposición de Material Escolar en agosto de 1885 en el Salón Central del Museo Nacional (actual Museo Nacional de Historia Natural de Chile). Esta exposición dio origen al primer Museo Pedagógico Chileno. Este último se montó en los salones del segundo piso de la Escuela de Aplicación Anexa a la Escuela Normal de Preceptoras Nº 1 y estuvo a cargo de Eduardo Rossig. En 1887 se formó una comisión para fomentar la actividad de este museo compuesta por José Abelardo Núñez, Inspector de las Escuelas Normales, Martín Schneider, Director de la Escuela Normal de Preceptores y Teresa Adametz, Directora de la Escuela Normal de Preceptoras. A pesar del trabajo de esta comisión, los escasos recursos con que se contaba para el mantenimiento del museo obligaron a cerrar sus puertas en 1890. Su colección se dispersó en 1892 entre diversos establecimientos educacionales.3​ En 1901 Jorge Figueroa, Inspector General de Instrucción Primaria, en un intento por revivir el Museo Pedagógico Chileno, inauguró la Exposición de Trabajos Manuales de las Escuelas Públicas de Santiago en un edificio del Estado que consiguió remodelar y amoblar. Además, logró que se nombrara un conservador para el museo y reunió un porcentaje importante del material que había formado parte del primer Museo Pedagógico Chileno. El 15 de agosto de 1902 se inauguró el “Museo y Biblioteca Pedagógicos”, institución de carácter socio-cultural orientada principalmente a los profesores. La colección de este museo se enriqueció con el material recolectado, más tarde, para la Exposición Internacional de Material de Enseñanza que se montó, el mismo año de la inauguración del museo, con motivo del Congreso Nacional de Enseñanza Pública. En 1904 se nombró una Junta de Vigilancia para darle un nuevo impulso al museo, la que estuvo conformada por Claudio Matte, Pedro Bannen, Manuel Salas L., Carlos Fernández Peña y Enrique Matta Vial. Bajo la dirección de Domingo Villalobos, conservador del museo, se le cambió el nombre llamándose, a partir de entonces, Museo de Educación Nacional. El museo se componía de obras pedagógicas y didácticas y de carácter científico además de algunas piezas de mobiliario escolar. El Conservador y los miembros de la Junta de Vigilancia, pusieron especial interés en la construcción de un local adecuado para el museo. Los planos se publicaron en la Revista de Instrucción Primaria en junio de 1906, pero por falta de recursos el museo cerró sus puertas ese mismo año.4​ La iniciativa que dio origen al Museo Pedagógico de Chile en 1941, vino de la mano del conservador, Carlos Stuardo Ortiz y del presidente de la época Pedro Aguirre Cerda, quién firmó el decreto de fundación del mismo, en el que se indicaba lo siguiente: “Créase, dependiente del Ministerio de Educación Pública, el Museo Pedagógico de Chile, organismo cuya misión será la de conservar, enriquecer, exhibir y divulgar todos aquellos antecedentes de carácter material, didáctico, intelectual o artístico, relacionados con la evolución de la enseñanza nacional…”5​");
            poi_10.put("lat", -33.441739);
            poi_10.put("lon", -70.678431);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject poi_11 = new JSONObject();
        try {
            poi_11.put("id", 11);
            poi_11.put("descripcion", "El Palacio La Alhambra, es un palacio de estilo neoárabe hispano-musulmán ubicado en la Calle Compañía Nº 1340, comuna de Santiago, en Santiago de Chile, diseñado por Manuel Aldunate, arquitecto chileno que viajó especialmente a España para estudiar el palacio-fortaleza original en Granada, Andalucía.");
            poi_11.put("nombre", "Palacio La Alhambra");
            poi_11.put("img", "https://upload.wikimedia.org/wikipedia/commons/3/32/PalacioLaAlhambra.JPG");
            poi_11.put("resenia", "Don Francisco Ignacio de Ossa y Mercado era minero y miembro de la Comisión Conservadora. Encargó la construcción de un monumento reluciente en Santiago al arquitecto Manuel Aldunate. Para ello, lo mandó a viajar por Europa y África, pudiendo así reunir las referencias de la Arquitectura islámica –específicamente del palacio de Alhambra de Granada y el Alcázar de Sevilla- necesarias lograr una réplica del Palacio de la Alhambra de Granada. Al momento en que el señor Ossa fallece, el palacio fue comprado por el señor Claudio Vicuña Guerrero –Ministro de Hacienda del gobierno del presidente Balmaceda- quién se preocupó de seguir adecuando el mobiliario al estilo Islámico mandando a hacer muebles en París. La propiedad estuvo a manos de él hasta el año 1891, cuando pasó a ser propiedad militar convirtiéndose en cuartel, perdiéndose el mobiliario debido a que fue saqueado. En el año 1900 el cuartel militar fue devuelto al señor Claudio Vicuña, quien tomó la decisión de poner en venta la propiedad que fue, finalmente, adquirida por don Julio Garrido Falcón, un filántropo chileno, con la condición de que se encargara de la restauración del monumento. Tuvo el Palacio hasta 1940 ya que no tenía más recursos para seguir la mantención del monumento y lo donó a su amigo Pedro Reszka Moreau quien era presidente de la Sociedad Nacional de Bellas Artes. El presidente Salvador Allende era un gran admirador de esta obra y, al mismo tiempo, amigo del presidente de la Sociedad Nacional de Bellas Artes, por ende, en el año 1973 fue declarada Monumento Nacional. Hoy en día, sigue bajo la Sociedad Nacional de Bellas Artes donde se imparten talleres de pintura, dibujos y exposiciones. Con los terremotos ocurridos en Chile, el palacio ha sufrido diversos daños. El que más le afectó fue el reciente 27-F (ocurrido el 27 de febrero de 2010), que generó muchos daños a nivel de decoración y tabiques siendo clausurado 2/3 del edificio -desde el gran salón hasta el patio final- por el Ministerio de Obras Públicas. El Ministerio de Vivienda y Urbanismo de Chile, el Consejo Nacional de la Cultura y las Artes, varias instituciones y ciertas Embajadas se encuentran haciendo donaciones para realizar los arreglos que necesita el palacio para volver a ser utilizado en su totalidad. En 2009, la embajada de Marruecos -a través de uno de sus consejeros, Abdelila Nejjari-, se comprometió a iniciar los trabajos de restauración del Palacio, aportando dineros para ello. Sin embargo, las diferentes áreas del Palacio la Alhambra resultaron severamente afectadas a causa del terremoto del 27 de febrero de 2010, debiéndose postergar el ofrecimiento del Reino de Marruecos hasta que la parte chilena resuelva el problema de los daños estructurales. El 11 de junio de 2011, la Dirección de Asuntos Culturales del Ministerio de Relaciones Exteriores de Chile, informó, en su página web oficial que. Tras contactos con el gobierno marroquí, el Rey Mohamed VI prometió como regalo Bicentenario un equipo de especialistas que vendría a realizar un trabajo muy fino en los decorados donde los versos del Corán aparecen escritos casi en filigrana. La única condición era que antes el palacio arreglara algunos problemas de techumbre para evitar filtraciones. Pero vino el terremoto del 2010 y esta vez las secuelas fueron estructurales. La mitad del palacio debió clausurarse por grietas profundas en sus muros y cielos en riesgo. Cayeron más yeserías...la venida de los marroquíes quedó postergada. Obviamente no se puede alhajar una construcción que debió cerrar la mitad de su edificio por daños mayores, pero Marruecos mantiene la oferta dijo Martín Donoso, el encargado del área Patrimonio de la Dirac”.1​La Dirac subrayó, en otra oportunidad, que En la restauración también jugará un rol importante el Reino de Marruecos, que con motivo del Bicentenario de la Independencia de Chile, ofreció generosamente apoyar el mejoramiento del edificio en su parte decorativa y su yesería de estilo filigranado nazarí- mudéjar. Esta etapa será realizada por una cuadrilla de yeseros especializados del reino alauí, los cuales, además, efectuarán talleres para transmitir estos conocimientos a restauradores y artesanos chilenos");
            poi_11.put("lat", -33.439286);
            poi_11.put("lon", -70.655987);

        } catch (JSONException e) {
            e.printStackTrace();
        }








        Array = new JSONArray();
        Array.put(poi_1);
        Array.put(poi_2);
        Array.put(poi_3);
        Array.put(poi_4);
        Array.put(poi_5);
        Array.put(poi_6);
        Array.put(poi_7);
        Array.put(poi_8);
        Array.put(poi_9);
        Array.put(poi_11);

    }


    public JSONArray getArray() {
        return Array;
    }
}
