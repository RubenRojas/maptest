package cl.ruben.maptest.view;

import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cl.ruben.maptest.R;
import cl.ruben.maptest.configClasses.Config;
import cl.ruben.maptest.interfaz.ViewDetallePoi;
import cl.ruben.maptest.model.PoiModel;
import cl.ruben.maptest.presenters.DetailPresenter;

public class PoiDetail extends AppCompatActivity implements ViewDetallePoi {

    DetailPresenter presenter;
    String TAG = Config.TAG;
    CollapsingToolbarLayout collapsingToolbar;
    PoiModel poi;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        //toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.);
        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.setExpanded(true);

        presenter = new DetailPresenter(this);
        String name = getIntent().getStringExtra("nombre");

        presenter.getPoi(name, PoiDetail.this);



    }

    @Override
    public void showData(PoiModel poiModel) {
        poi = poiModel;
        collapsingToolbar.setTitle(poi.getNombre());

        TextView resenia = (TextView)findViewById(R.id.resenia);
        resenia.setText(poi.getResenia());

        TextView descripcion = (TextView)findViewById(R.id.descripcion);
        descripcion.setText(poi.getDescripcion());



        ImageView poiImg = (ImageView)findViewById(R.id.poiImg);
        Picasso.get()
                .load(poi.getImg())
                .resize(1080,720)
                .placeholder(R.drawable.progress)
                .centerCrop()
                .noFade()

                .into(poiImg);


    }
}