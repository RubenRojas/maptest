package cl.ruben.maptest.view;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import cl.ruben.maptest.R;
import cl.ruben.maptest.configClasses.Config;
import cl.ruben.maptest.interfaz.ViewMapa;
import cl.ruben.maptest.model.PoiModel;
import cl.ruben.maptest.presenters.MapaPresenter;
import cl.ruben.maptest.view.adapters.PopupAdapter;

public class Mapa extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        ViewMapa {

    private GoogleMap mMap;
    MapaPresenter mapaPresenter;
    String TAG = Config.TAG;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        //instancia del PoiPresenter


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapaPresenter = new MapaPresenter(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Config.santiago, 13));

        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));

        //solicito el listado de POI al modelo, mediante el presenter
        Log.d(TAG, "onMapReady: SOLICITANDO POI");
        mapaPresenter.getPoiList(Mapa.this);


    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.d(Config.TAG, "onInfoWindowClick: "+marker.toString());
        Log.d(TAG, "onInfoWindowClick: "+marker.getTitle());
        Log.d(TAG, "onInfoWindowClick: "+marker.getSnippet());

        Intent i = new Intent(Mapa.this, PoiDetail.class);
        i.putExtra("nombre", marker.getTitle());
        startActivity(i);

    }


    @Override
    public void displayPoiList(List<PoiModel> list) {
        for (int i=0; i<list.size();i++){
            PoiModel tmp = list.get(i);
            String descr = tmp.getDescripcion().substring(0,100)+" [...]";
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(tmp.getLat(), tmp.getLon()))
                    .snippet(descr)
                    .title(tmp.getNombre()));
        }
    }
}