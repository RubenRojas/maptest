package cl.ruben.maptest.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import cl.ruben.maptest.model.PoiData;
import cl.ruben.maptest.R;
import cl.ruben.maptest.interfaz.ViewSplash;
import cl.ruben.maptest.presenters.SplashPresenter;

public class Splash extends AppCompatActivity implements ViewSplash {

    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new SplashPresenter(this);
        presenter.insertPois(new PoiData().getArray(), Splash.this);
    }


    @Override
    public void redirect() {
        startActivity(new Intent(Splash.this, Mapa.class));
        this.finish();
    }
}