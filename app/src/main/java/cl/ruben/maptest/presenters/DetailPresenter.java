package cl.ruben.maptest.presenters;

import android.content.Context;

import cl.ruben.maptest.interfaz.ViewDetallePoi;
import cl.ruben.maptest.model.PoiModel;

public class DetailPresenter {
    private ViewDetallePoi view;
    private PoiModel model;

    public DetailPresenter(ViewDetallePoi view) {
        this.view = view;
        this.model = new PoiModel();

    }

    public void getPoi(String nombre, Context ctx){
        PoiModel poi = model.getPoiByName(nombre, ctx);
        view.showData(poi);
    }
}
