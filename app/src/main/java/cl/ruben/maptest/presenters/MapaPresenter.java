package cl.ruben.maptest.presenters;

import android.content.Context;
import android.util.Log;

import java.util.List;

import cl.ruben.maptest.configClasses.Config;
import cl.ruben.maptest.interfaz.ViewMapa;
import cl.ruben.maptest.model.PoiModel;

public class MapaPresenter {

    private PoiModel poiModel;
    private ViewMapa viewMapa;
    String TAG = Config.TAG;

    public MapaPresenter(ViewMapa viewMapa) {
        this.viewMapa = viewMapa;
        this.poiModel = new PoiModel();
    }

    public void getPoiList(Context ctx){
        Log.d(TAG, "getPoiList: LLAMADA EN EL PRESENTER");
        List<PoiModel> listado = poiModel.getList(ctx);
        viewMapa.displayPoiList(listado);


    }

}
