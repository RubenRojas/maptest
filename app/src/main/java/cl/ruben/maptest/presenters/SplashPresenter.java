package cl.ruben.maptest.presenters;

import android.content.Context;

import org.json.JSONArray;

import cl.ruben.maptest.configClasses.Config;
import cl.ruben.maptest.interfaz.ViewSplash;
import cl.ruben.maptest.model.PoiModel;

public class SplashPresenter {

    private PoiModel poiModel;
    private ViewSplash viewSplash;
    String TAG = Config.TAG;

    public SplashPresenter(ViewSplash viewSplash) {
        this.viewSplash = viewSplash;
        this.poiModel = new PoiModel();
    }

    public void insertPois(JSONArray PoiArray, Context ctx){
       //inserto los poi en la BD mediante el modelo
       //redirecciono usando la funcion de la vista
        poiModel.insertPois(PoiArray, ctx);

       viewSplash.redirect();
    }

}
